<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/index', 'HomeController@indexApplyLeave')->name('home.leave');
Route::get('/employee/data', 'DataCenter@EmployesActive')->name('employee.data.active');
Route::get('/employee/undata', 'DataCenter@EmployesNonActive')->name('employee.data.nonActive');
Route::get('/employes/summary', 'DataSummaryController@summaryEmployes')->name('employes.summary.index');

Route::resource('/employee', 'EmployeeController');