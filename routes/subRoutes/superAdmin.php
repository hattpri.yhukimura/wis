<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home.index');

Route::get('/employes/summary', 'DataSummaryController@summaryEmployes')->name('employes.summary');

Route::get('/user/data', 'DataCenter@registrationUser')->name('user.data');
Route::get('/employee/data', 'DataCenter@Employes')->name('employee.data');

Route::post('/employee/{id}/action', 'ActionCenterController@employeStatus')->name('employes.status');

Route::resource('/user', 'RegistrationUserController');
Route::resource('/employee', 'EmployeeController');
