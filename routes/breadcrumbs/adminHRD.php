<?php 

Breadcrumbs::for('admin.hrd.home.index', function ($trail) {
    $trail->push('Dashboard', route('admin.hrd.home.index'));
});

//Employes
Breadcrumbs::for('admin.hrd.employee.index', function ($trail) {
    $trail->push('Dashboard', route('admin.hrd.home.index'));
    $trail->push('Employes', route('admin.hrd.employee.index'));
});
Breadcrumbs::for('admin.hrd.employee.create', function ($trail) {
    $trail->push('Dashboard', route('admin.hrd.home.index'));
    $trail->push('Employes', route('admin.hrd.employee.index'));
    $trail->push('Create', route('admin.hrd.employee.create'));
});
Breadcrumbs::for('admin.hrd.employee.edit', function ($trail, $user) {
    $trail->push('Dashboard', route('admin.hrd.home.index'));
    $trail->push('Employes', route('admin.hrd.employee.index'));
    $trail->push('Edit', route('admin.hrd.employee.edit', [$user]));
});

//Summary
Breadcrumbs::for('admin.hrd.employes.summary.index', function ($trail) {
    $trail->push('Dashboard', route('admin.hrd.home.index'));
    $trail->push('Summary', route('admin.hrd.employes.summary.index'));
});

//Leave
Breadcrumbs::for('admin.hrd.home.leave', function ($trail) {
    $trail->push('Dashboard', route('admin.hrd.home.index'));
    $trail->push('Apply Leave', route('admin.hrd.home.leave'));
});