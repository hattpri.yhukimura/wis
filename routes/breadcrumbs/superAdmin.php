<?php 

Breadcrumbs::for('superAdmin.home.index', function ($trail) {
    $trail->push('Dashboard', route('superAdmin.home.index'));
});

// Registration user
Breadcrumbs::for('superAdmin.user.index', function ($trail) {
	$trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Register', route('superAdmin.user.index'));
});
Breadcrumbs::for('superAdmin.user.create', function ($trail) {
    $trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Register', route('superAdmin.user.index'));
    $trail->push('Create', route('superAdmin.user.create'));
});
Breadcrumbs::for('superAdmin.user.edit', function ($trail, $user) {
	$trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Register', route('superAdmin.user.index'));
    $trail->push('Edit', route('superAdmin.user.edit', [$user]));
});

// Employee
Breadcrumbs::for('superAdmin.employee.index', function ($trail) {
	$trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Employee', route('superAdmin.employee.index'));
});
Breadcrumbs::for('superAdmin.employee.create', function ($trail) {
	$trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Employee', route('superAdmin.employee.index'));
    $trail->push('Create', route('superAdmin.employee.create'));
});
Breadcrumbs::for('superAdmin.employee.edit', function ($trail, $id) {
    $trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Employee', route('superAdmin.employee.index'));
    $trail->push('Edit', route('superAdmin.employee.edit', [$id]));
});

//Summary Employes
Breadcrumbs::for('superAdmin.employes.summary', function ($trail) {
    $trail->push('Dashboard', route('superAdmin.home.index'));
    $trail->push('Summary', route('superAdmin.employes.summary'));
});
