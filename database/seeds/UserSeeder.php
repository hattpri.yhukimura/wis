<?php

use Illuminate\Database\Seeder;
Use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = User::create([
	       	'username' => 'super.admin',
	        'email' => 'dede-d5406a@inbox.mailtrap.io',
	        'email_verified_at' => now(),
	        'password' => Hash::make('12345678'),
          'access_id' => 1,
          'actived'   => 1,
       	
      	 ]);

       $superAdmin->assignRole('superAdmin');

       $admin = User::create([
	       	'username' => 'admin.hr',
	        'email' => 'dede.aftafiandi@frameworks-studios.com',
	        'email_verified_at' => now(),
	        'password' => Hash::make('12345678'),
       	
      	 ]);

       $admin->assignRole('admin');
    }
}