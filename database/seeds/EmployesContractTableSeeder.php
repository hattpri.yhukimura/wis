<?php

use Illuminate\Database\Seeder;
use App\Models\Employes;
use Illuminate\Support\Arr;


class EmployesContractTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $gender = ['Male', 'Female'];
        $education = ['SD', 'SMP', 'SMA', 'Ahli Madya (D3)', 'S1', 'S2', 'S3']; 
        $marital_status = ['Single', 'Married', 'Widow/Widower'];
        $religion = ['Islam', 'Protestan', 'Katolik', 'Hindu', 'Buddha', 'Khonghucu'];   
        $bulan = ['+1 month', '+2 month', '+3 month', '+6 month'];

       for ($i=1; $i <=100 ; $i++) { 
           Employes::create([
              'nik' => $i,
              'first_name' => $faker->sentence(1),
              'last_name'	=> $faker->sentence(2),
              'department_id' => rand(1, 9),
              'email' => $faker->email,
              'position' => $faker->sentence(2),
              'emp_status' => 'contract',
              'nationality' => $faker->state,
              'start_date' => $faker->date('Y-m-d'),
              'end_date'	=> $faker->date('Y-m-d', strtotime(Arr::random($bulan))),
              'birthday'  => $faker->date('Y-m-d'),
              'hometown' => $faker->sentence(2),
              'maiden_name' => $faker->name,
              'gender' => Arr::random($gender),
              'id_card' => $i,
              'phone' => $faker->phoneNumber,
              'address' => $faker->address,
              'provinsi' => rand(11, 19),
              'town' => $faker->city,
              'education' => Arr::random($education),
              'marital_status' => Arr::random($marital_status),
              'npwp' => $faker->randomDigit,
              'kk'	=> $faker->randomDigit,
              'religion' => Arr::random($religion),
              'bpjs_ketenagakerjaan' => $faker->randomDigit,
              'bpjs_kesehatan' => $faker->randomDigit,
              'photo'	=> null,
              'project_id' => rand(1, 10),
              'emp_active' => rand(0, 1),
           ]);
       }
    }
}
