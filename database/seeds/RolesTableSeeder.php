<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'name' => 'superAdmin'
        ]);

        Role::create([
        	'name' => 'admin'
        ]);

        Role::create([
        	'name' => 'it'
        ]);

        Role::create([
        	'name' => 'hr'
        ]);

        Role::create([
        	'name' => 'finance'
        ]);

        Role::create([
        	'name' => 'production'
        ]);

        Role::create([
        	'name' => 'operation'
        ]);

        Role::create([
        	'name' => 'pipeline'
        ]);

        Role::create([
        	'name' => 'facility'
        ]);

        Role::create([
        	'name' => 'outsource'
        ]);

        Role::create([
        	'name' => 'pkl'
        ]);

        Role::create([
        	'name' => 'management'
        ]);
    }
}
