<?php

use Illuminate\Database\Seeder;
use App\Models\Access;

class AccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {     
        Access::create([
            'accessing' => 'Administration'
        ]);  
        Access::create([
        	'accessing' => 'Head Manager'
        ]);
        Access::create([
        	'accessing' => 'Producer'
        ]);
        Access::create([
        	'accessing' => 'Project Manager'
        ]);
        Access::create([
        	'accessing' => 'Coordinator'
        ]);
        Access::create([
        	'accessing' => 'Supervisor'
        ]);
        Access::create([
        	'accessing' => 'Leader'
        ]);
        Access::create([
        	'accessing' => 'staff'
        ]);
    }
}
