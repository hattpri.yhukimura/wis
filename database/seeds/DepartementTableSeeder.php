<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Department::create([
        	'name' 		=> 'Information Technology',
        	'code_name'	=> 'IT',
        ]);
        Department::create([
        	'name' 		=> 'Facility',
        	'code_name'	=> 'Facility',
        ]);
        Department::create([
        	'name' 		=> 'Finance & Accounting',
        	'code_name'	=> 'FA',
        ]);
        Department::create([
        	'name' => 'Human Resources',
        	'code_name'	=> 'HR',
        ]);
        Department::create([
        	'name' => 'Production',
        	'code_name'	=> 'Production',
        ]);
        Department::create([
        	'name' => 'Pipeline',
        	'code_name'	=> 'Pipeline',
        ]);
        Department::create([
        	'name' => 'Operation',
        	'code_name'	=> 'Operation',
        ]);
        Department::create([
        	'name' => 'Management',
        	'code_name'	=> 'Management',
        ]);
         Department::create([
        	'name' => 'Outsource',
        	'code_name'	=> 'Outsource',
        ]);
    }
}
