<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            DepartementTableSeeder::class,
         	RolesTableSeeder::class,           
            AccessTableSeeder::class,
            ProjectSeeder::class,                   
         	UserSeeder::class,
            EmployeeSeeder::class,           
         ]);
    }
}
