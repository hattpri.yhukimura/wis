<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeptIdLeaveIdOnEmployesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employes', function (Blueprint $table) {
            // $table->unsignedBigInteger('leave_id')->nullable()->default(null)->after('user_id');
            $table->unsignedBigInteger('department_id')->nullable()->default(null)->after('email');
            $table->unsignedBigInteger('rusun_id')->nullable()->default(null)->after('bpjs_kesehatan');
            $table->unsignedBigInteger('project_id')->nullable()->default(null)->after('rusun_id');
            $table->string('photo')->nullable()->default(null)->after('project_id');

            // $table->foreign('leave_id')->references('id')->on('leaves')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('rusun_id')->references('id')->on('dormitories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employes', function (Blueprint $table) {
            //
        });
    }
}
