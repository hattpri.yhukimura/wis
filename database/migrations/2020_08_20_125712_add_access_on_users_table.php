<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccessOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('access_id')->nullable()->default(null)->after('password');
            $table->unsignedBigInteger('employes_id')->unique()->nullable()->default(null)->after('id');            

            $table->foreign('access_id')->references('id')->on('accesses'); 
            $table->foreign('employes_id')->references('id')->on('employes');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('access_id');
        });
    }
}
