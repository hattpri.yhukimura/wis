<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            // $table->bigInteger('leave_id')->unique()->nullable()->default(null);
            $table->bigInteger('nik')->unique();
            $table->string('first_name');
            $table->string('last_name')->nullable()->default(null);
            $table->string('email')->default(null);
            // $table->bigInteger('department_id');
            $table->string('position');
            $table->string('emp_status');
            $table->string('nationality')->nullable()->default(null);
            $table->date('start_date');
            $table->date('end_date')->nullable()->default(null);
            $table->date('birthday');
            $table->string('hometown')->nullable()->default(null);
            $table->string('maiden_name')->nullable()->default(null);
            $table->string('gender');
            $table->string('id_card')->unique();
            $table->string('phone')->nullable()->default(null);
            $table->text('address')->nullable()->default(null);
            $table->string('provinsi')->nullable()->default(null);
            $table->string('town')->nullable()->default(null);
            $table->string('education')->nullable()->default(null);
            $table->string('marital_status')->nullable()->default(null);
            $table->bigInteger('npwp')->nullable()->default(null);
            $table->bigInteger('kk')->nullable()->default(null);
            $table->string('religion')->nullable()->default(null);
            $table->bigInteger('bpjs_ketenagakerjaan')->nullable()->default(null);
            $table->bigInteger('bpjs_kesehatan')->nullable()->default(null);
            // $table->bigInteger('rusun_id')->nullable()->default(null);
            // $table->bigInteger('project_id')->nullable()->default(null);
            $table->integer('annual')->nullable()->default(0);
            $table->integer('exdo')->nullable()->default(0);
            $table->integer('total_annual')->nullable()->default(0);
            $table->integer('total_exdo')->nullable()->default(0);
            $table->boolean('emp_active')->nullable()->default(false);
            $table->string('created_by')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
