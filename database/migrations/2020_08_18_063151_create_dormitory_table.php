<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDormitoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dormitories', function (Blueprint $table) {
            $table->id();           
            $table->unsignedBigInteger('employes_id')->nullable()->default(null);
            $table->string('room');
            $table->string('status');
            $table->boolean('active')->default(false);
            $table->string('created_by')->nullable()->default(null);
            $table->timestamps();
           
            $table->foreign('employes_id')->references('id')->on('employes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dormitories');
    }
}
