<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Employes;
use Faker\Generator as Faker;

$factory->define(Employes::class, function (Faker $faker) {	
    return [    	
       'nik' => rand(1, 2000),
       'first_name' => $faker->sentence(1),
       'last_name'	=> $faker->sentence(2),
       'department_id' => rand(1, 9),
       'position' => $faker->sentence(2),
       'emp_status' => 'contract',
       'nationality' => $faker->state,
       'start_date' => '2020-01-01',
       'end_date'	=> '2020-12-30',
       'birthday'  => $faker->date('Y-m-d'),
       'hometown' => $faker->sentence(2),
       'maiden_name' => $faker->name,
       'gender' => rand(1, 2),
       'id_card' => rand(1, 2000),
       'phone' => $faker->phoneNumber,
       'address' => $faker->address,
       'provinsi' => $faker->country,
       'town' => $faker->city,
       'education' => rand(1, 5),
       'marital_status' => rand(1, 4),
       'npwp' => $faker->randomDigit,
       'kk'	=> $faker->randomDigit,
       'religion' => rand(1, 6),
       'bpjs_ketenagakerjaan' => $faker->randomDigit,
       'bpjs_kesehatan' => $faker->randomDigit,
    ];
});
