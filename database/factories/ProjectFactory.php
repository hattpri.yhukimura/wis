<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
       'name' => $faker->sentence(1),
       'code' => rand(1, 10),
       'active' => rand(0,1)
    ];
});
