@extends('superAdmin.templates.default')

@section('content')

<div class="row">
	<div class="col-sm-4">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fas fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Employes</span>
          <span class="info-box-number">{{ $employes->count() }}</span>
          <span class="progress-description" style="text-align: right; font-size: 12px;">
            <a href="#" class="text-white">Detail</a>
          </span>
        </div>
      </div>
	</div>
	<div class="col-sm-4">
      <div class="info-box bg-blue">
        <span class="info-box-icon"><i class="far fa-calendar-alt"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Employee Contract</span>
          <span class="info-box-number">{{ $employes->count() }}</span>
          <span class="progress-description">
            {{ $percentEmployes }}% Increase in 30 Days
          </span>
        </div>
      </div>
	</div>
</div>

@endsection