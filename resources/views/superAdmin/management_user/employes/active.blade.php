@if($model->emp_active === 0) 
	<a href="{{ route('superAdmin.employes.status', $model) }}" class="text-red" id="actived">Deactived</a>

@elseif($model->emp_active === 1) 
	<a href="{{ route('superAdmin.employes.status', $model) }}"class="text-green" id="actived">Actived</a>

@endif

<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    $('a#actived').on('click', function(e){
        e.preventDefault();
       
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "You will not be able to return it",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, Do it!',
		  cancelButtonText: 'No, Cancel!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('activeForm').action = href;
            document.getElementById('activeForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Deleted!',
		      'Data deleted successfully from system.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'Data safed :)',
		      'error'
		    )
		  }
		});        
    });
   
</script>