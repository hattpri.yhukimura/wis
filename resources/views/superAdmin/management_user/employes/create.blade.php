@extends('superAdmin.templates.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">Create Data Employee</h3>             
    </div>
    <div class="card-body">
		<form method="POST" action="{{ route('superAdmin.employee.store') }}" enctype="multipart/form-data">
			@csrf
		  <div class="form-group row">
		    <label for="nik" class="col-sm-2 col-form-label">NIK</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('nik') is-invalid @enderror" id="nik" value="{{ old('nik') }}" name="nik">
		        @error('nik') 
				   <span class="help-block text-red">{{ $message }}</span>	
				@enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" value="{{ old('first_name') }}" name="first_name">
		      @error('first_name') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div> 
		  <div class="form-group row">
		    <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="last_name" value="{{ old('last_name') }}" name="last_name">
		    </div>
		  </div> 
		  <div class="form-group row">
		    <label for="email" class="col-sm-2 col-form-label">Email</label>
		    <div class="col-sm-10">
		      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" name="email">
		      @error('email') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="position" class="col-sm-2 col-form-label">Position</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('position') is-invalid @enderror" id="position" value="{{ old('position') }}" name="position">
		      @error('email') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="department" class="col-sm-2 col-form-label">Department</label>
		    <div class="col-sm-10">
		      <select name="department" class="form-control @error('department') is-invalid @enderror select2">
		      	<option value="">Open this name departement</option>
		      	@foreach($departments as $department)
		      	<option value="{{ $department->id }}">{{ $department->code_name }}</option>
		      	@endforeach
		      </select>
		      @error('department') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="emp_status" class="col-sm-2 col-form-label">Employee Status</label>
		    <div class="col-sm-10">
		      <select name="emp_status" id="emp" class="form-control @error('emp_status') is-invalid @enderror" onclick="annual()">
		      	<option value="">Open this employee status</option>
		      	<option value="Contract">Contract</option>
		      	<option value="Intership">Intership</option>		      
		      	<option value="Outsurce">Outsurce</option>
		      	<option value="Permanent" id="de">Permanent</option>
		      </select>
		      @error('emp_status') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="start_date" class="col-sm-2 col-form-label">Join Date</label>
		    <div class="col-sm-10">
		      <input type="date" class="form-control @error('start_date') is-invalid @enderror" id="start_date" value="{{ old('start_date') }}" name="start_date">
		      @error('start_date') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row" id="end">
		    <label for="end_date" class="col-sm-2 col-form-label">End Date</label>
		    <div class="col-sm-10">
		      <input type="date" class="form-control" id="end_date" value="{{ old('end_date') }}" name="end_date">
		    </div>
		  </div>
		  <div class="form-group row" id="annual">
		   
		  </div>
		  <div class="form-group row">
		    <label for="nationality" class="col-sm-2 col-form-label">Citizenship</label>
		    <div class="col-sm-10">
		      <input type="text" name="nationality" class="form-control @error('nationality') is-invalid @enderror" id="nationality" value="{{ old('nationality') }}">
		      @error('nationality') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="hometown" class="col-sm-2 col-form-label">Hometown</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('hometown') is-invalid @enderror" id="hometown" value="{{ old('hometown') }}" name="hometown">
		      @error('hometown') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror 
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="birthday" class="col-sm-2 col-form-label">Date of birth</label>
		    <div class="col-sm-10">
		      <input type="date" class="form-control @error('birthday') is-invalid @enderror" id="birthday" value="{{ old('birthday') }}" name="birthday">
		      @error('birthday') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="maiden_name" class="col-sm-2 col-form-label">Maiden Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('maiden_name') is-invalid @enderror" id="maiden_name" value="{{ old('maiden_name') }}" name="maiden_name">
		      @error('maiden_name') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="gender" class="col-sm-2 col-form-label">Gender</label>
		    <div class="col-sm-10 @error('maiden_name') is-invalid @enderror">
		        <div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="gender" id="gender1" value="Male">
				  <label class="form-check-label" for="inlineRadio1">Male</label>
				</div>
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="gender" id="gender2" value="Female">
				  <label class="form-check-label" for="inlineRadio2">Female</label>
				</div>
				@error('maiden_name') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror				
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="id_card" class="col-sm-2 col-form-label">ID Card (KTP)</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('id_card') is-invalid @enderror" id="id_card" value="{{ old('id_card') }}" name="id_card">
		      @error('id_card') 
				   <span class="help-block text-red">{{ $message }}</span>	
			  @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone') }}" name="phone">
		    	@error('phone') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="address" class="col-sm-2 col-form-label">Address</label>
		    <div class="col-sm-10">
		     <textarea name="address" class="form-control @error('address') is-invalid @enderror">{{ old('address') }}</textarea>
		        @error('address') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="province" class="col-sm-2 col-form-label">Province</label>
		    <div class="col-sm-10">
		     <select name="province" class="form-control @error('province') is-invalid @enderror select2">
		     	<option value="">Open this name province</option>
			     @foreach ($provinces as $province)
			     	<option value="{{ $province['id'] }}">{{ $province['nama'] }}</option>
			     @endforeach
		     </select>
			      @error('province') 
				   <span class="help-block text-red">{{ $message }}</span>	
			      @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="town" class="col-sm-2 col-form-label">Town</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('town') is-invalid @enderror" id="town" value="{{ old('town') }}" name="town">
		    	@error('town') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="education" class="col-sm-2 col-form-label">Education</label>
		    <div class="col-sm-10">
		      <select name="education" class="form-control @error('education') is-invalid @enderror">
		      	<option value="">Open this title education</option>
		      	<option value="SD">SD</option>
		      	<option value="SMP">SMP</option>
		      	<option value="SMA">SMA</option>
		      	<option value="Ahli Madya (D3)">Ahli Madya (D3)</option>
		      	<option value="S1">S1</option>
		      	<option value="S2">S2</option>
		      	<option value="S3">S3</option>
		      </select>
		        @error('education') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="marital_status" class="col-sm-2 col-form-label">Marital Status</label>
		    <div class="col-sm-10">
		      <select name="marital_status" class="form-control @error('marital_status') is-invalid @enderror">
		      	<option value="">Open this marital status</option>
		      	<option value="Single">Single</option>
		      	<option value="Married">Married</option>
		      	<option value="Widow/Widower">Widow/Widower</option>
		      </select>
		        @error('marital_status') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="npwp" class="col-sm-2 col-form-label">NPWP</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('npwp') is-invalid @enderror" id="npwp" value="{{ old('npwp') }}" name="npwp">
		    	@error('npwp') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="kk" class="col-sm-2 col-form-label">KK</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('kk') is-invalid @enderror" id="kk" value="{{ old('kk') }}" name="kk">
		    	@error('kk') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="kesehatan" class="col-sm-2 col-form-label">BPJS Kesehatan</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('kesehatan') is-invalid @enderror" id="kesehatan" value="{{ old('kesehatan') }}" name="kesehatan">
		    	@error('kesehatan') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="ketenagakerjaan" class="col-sm-2 col-form-label">BPJS Ketenagakerjaan</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control @error('ketenagakerjaan') is-invalid @enderror" id="ketenagakerjaan" value="{{ old('ketenagakerjaan') }}" name="ketenagakerjaan">
		       @error('ketenagakerjaan') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="religion" class="col-sm-2 col-form-label">Religion</label>
		    <div class="col-sm-10">
		      <select name="religion" class="form-control @error('religion') is-invalid @enderror">
		      	<option value="">Open this religion status</option>
		      	<option value="Islam">Islam</option>
		      	<option value="Protestan">Protestan</option>
		      	<option value="Katolik">Katolik</option>
		      	<option value="Hindu">Hindu</option>
		      	<option value="Buddha">Buddha</option>
		      	<option value="Khonghucu">Khonghucu</option>
		      </select>
		        @error('religion') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="project" class="col-sm-2 col-form-label">Project</label>
		    <div class="col-sm-10">
		      <select name="project" class="form-control select2 @error('project') is-invalid @enderror">
		      	<option value="">Open this menu project</option>
		      	@foreach ($projects as $project)
		      		<option value="{{ $project->id }}">{{ $project->name }}</option>
		      	@endforeach
		      </select>
		       @error('project') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="photo" class="col-sm-2 col-form-label">Photo</label>
		    <div class="col-sm-10">
		      <input class="form-control @error('photo') is-invalid @enderror" name="photo" type="file" value="{{ old('photo') }}">
		    	@error('photo') 
				   <span class="help-block text-red">{{ $message }}</span>	
			    @enderror
		    </div>
		  </div>

		  <div class="form-group row">
		    <div class="col-sm-12">
		      <button type="submit" class="btn btn-outline-success btn-sm float-right">Add</button>
		    </div>
		  </div>

		</form>
    </div>
</div>
@endsection

@push('select2')
	<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush

@push('jquery')
	<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
@endpush

@push('scripts')
	<script>
		$('.select2').select2();		

		function annual() {

			var pemanentValue = $("#emp").val();
			
			if (pemanentValue === "Permanent") {
				document.getElementById('end').classList.remove("mystyle");
				
				document.getElementById('annual').innerHTML= `
				<label for="costAnnual" class="col-sm-2 col-form-label">Annual</label>
				    <div class="col-sm-10">
				      <input type="text" name="costAnnual" class="form-control" id="costAnnual" value="0">
				    </div>
			`;
			}else{
				document.getElementById('annual').innerHTML= "";
			}
		}
	</script>
@endpush