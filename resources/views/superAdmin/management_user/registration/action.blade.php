<a href="{{ route('superAdmin.user.edit', $model) }}" class="btn btn-xs btn-outline-warning">Edit</a>
@if($model->employes_id !== null)
<button href="{{ route('superAdmin.user.show', $model) }}" type="button" class="btn btn-xs btn-outline-info" data-toggle="modal" data-target="#showEmployes" id="show">Show</button>
@endif
<button href="{{ route('superAdmin.user.destroy', $model) }}" class="btn btn-xs btn-outline-danger" id="delete">Hapus</button>

<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    $('button#delete').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "You will not be able to return it",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, Do it!',
		  cancelButtonText: 'No, Cancel!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('deleteForm').action = href;
            document.getElementById('deleteForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Deleted!',
		      'Data deleted successfully from system.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'Data safed :)',
		      'error'
		    )
		  }
		});        
    });

    $('button#show').on('click', function(a){
    	a.preventDefault();
    	var href = $(this).attr('href');    

    	$.ajax({
    		url: href,
    		data: 'get',
    		success: function(b){    		
    			$("#contentModal").html(b);    			
    		}
    	});    	

    });
</script>