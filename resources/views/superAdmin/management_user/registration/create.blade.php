@extends('superAdmin.templates.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">Registration User</h3>
    </div>
    <div class="card-body col-sm-12 col-md-12">
	
	<form action="{{ route('superAdmin.user.store') }}" method="POST">
		@csrf		
	  <div class="form-group row">
	    <label for="username" class="col-sm-2 col-form-label">Username</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" value="{{ old('username') }}" name="username">
	        @error('username')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			@enderror
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="employee" class="col-sm-2 col-form-label">Employee</label>
	    <div class="col-sm-10">
	       <select class="custom-select select2 @error('employee') is-invalid @enderror" name="employee">
			  <option value="">Open this select employee</option>
			  @foreach($employes as $employee)
				  <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }}</option>		
			  @endforeach
			</select>
			@error('employee')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			@enderror
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="password" class="col-sm-2 col-form-label">Password</label>
	    <div class="col-sm-10">
	    	<input type="input" class="form-control @error('password') is-invalid @enderror" id="password" value="Batam{{ date('Y') }}" name="password" readonly="true">
	        @error('password')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			@enderror
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="email" class="col-sm-2 col-form-label">Email</label>
	    <div class="col-sm-10">
	      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" name="email">
	        @error('email')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			@enderror
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="actived" class="col-sm-2 col-form-label">Active</label>
	    <div class="col-sm-10">
		     <div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="actived" id="inlineRadio1" value="1">
			  <label class="form-check-label" for="inlineRadio1">Yes</label>
			</div>
			<div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="actived" id="inlineRadio2" value="0">
			  <label class="form-check-label" for="inlineRadio2">No</label>
			</div>
			@error('actived')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			@enderror
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="access" class="col-sm-2 col-form-label">Access</label>
	    <div class="col-sm-10">
	      <select class="custom-select select2 @error('access') is-invalid @enderror" name="access" >
			  <option value="">Open this select access</option>

			  @foreach($accesses as $access)
			  <option value="{{ $access->id }}">{{ $access->accessing }}</option>
			  @endforeach

			  @error('access')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			  @enderror
			</select>
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="department" class="col-sm-2 col-form-label">Department</label>
	    <div class="col-sm-10">
	      <select class="custom-select select2 @error('department') is-invalid @enderror" name="department" >
			  <option value="">Open this select department</option>

			  @foreach($departments as $department)
			  <option value="{{ $department->id }}">{{ $department->code_name }}</option>
			  @endforeach

			  @error('department')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			  @enderror
			</select>
	    </div>
	  </div>
	  <div class="form-group row">
	    <label for="roles" class="col-sm-2 col-form-label">Role</label>
	    <div class="col-sm-10">
	      <select class="custom-select select2 @error('roles') is-invalid @enderror" name="roles">
			  <option value="">Open this select role</option>

			  @foreach($roles as $role)
			  <option value="{{ $role->name }}">{{$role->name}}</option>
			  @endforeach

			</select>
			  @error('roles')
			   <span class="invalid-feedback text-sm" role="alert">{{ $message }}</span>
			  @enderror
	    </div>
	  </div>
	  <div class="form-group row">	    
	    <div class="col-sm-12">
	      <button class="submit btn btn-success btn-sm float-right">save</button>
	    </div>
	  </div>

	</form>

    </div>
</div>
@endsection
@push('select2')
	<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush

@push('jquery')
	<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
@endpush

@push('scripts')
	<script>
		$('.select2').select2();
	</script>
@endpush