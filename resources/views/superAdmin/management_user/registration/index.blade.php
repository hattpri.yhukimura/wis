@extends('superAdmin.templates.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">Data Registration</h3> 
         <a href="{{ route('superAdmin.user.create') }}" class="btn btn-sm btn-outline-primary float-right">Add</a>        
    </div>
    <div class="card-body">
    	<div class="table-reponsive table-sm">
			<table id="registrationData" class="table table-bordered table-hover text-sm">
			 	<thead>
			 		<tr>
			 			<th>No</th>
			 			<th>Username</th>
			 			<th>Employe Id</th>
			 			<th>Email</th>
			 			<th>Active</th>
			 			<th>Access</th>
			 			<th>Role</th>
			 			<th>Department</th>
			 			<th>Action</th>
			 		</tr>
			 	</thead>		 	
			 </table>
		 </div>
    </div>
</div>
<form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Hapus" style="display: none">
</form>
<div class="modal fade bd-example-modal-lg" id="showEmployes" tabindex="-1" role="dialog" aria-labelledby="showEmployesLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document" id="contentModal">
  	    
  </div>
</div>
@endsection
@push('styles')
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">
@endpush


@push('jquery')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>	
	
	{{-- fitur excel --}}
	<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/DataTables/JSZip/jszip.min.js') }}"></script>
@endpush

@push('scripts')
	@include('superAdmin.templates.partials.alerts')
<script>
	$(function () {
		$('#registrationData').DataTable({
		 		processing: true,
                serverSide: true,
                ajax: '{{ route('superAdmin.user.data') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},                
                    { data: 'username'} ,
                    { data: 'employes_id'},
                    { data: 'email'},
                    { data: 'actived'},   
                    { data: 'access_id'},
                    { data: 'roles'},
                    { data: 'department'},
                    { data: 'action'},         
                ],
			    dom: 'Bfrtip',
		        buttons: [
		             'excel'
		        ]                 
		});
	});
</script>
@endpush