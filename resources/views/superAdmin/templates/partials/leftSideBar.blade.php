 <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="{{ route('superAdmin.home.index') }}" class="brand-link">     
     <img src="{{ asset('assets/dist/imgs/logo/wis_logo.png') }}" alt="AdminLTE Logo" class="brand-image img-rounded elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light text-sm">Wide Information System</span>
    </a>

  
    <div class="sidebar">
      
     <div class="user-panel mt-3 pb-3 mb-3 d-flex">            
       <div class="image">
          <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-3" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ route('superAdmin.home.index') }}" class="d-block">{{ auth()->user()->username }}</a>
        </div>
      </div>
    
      <nav class="mt-2">
      <div class="user-panel">
        <span class="brand-text font-weight-light text-sm text-white">HRD Menu</span>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">         
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Management User
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('superAdmin.user.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registration User</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('superAdmin.employee.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Employee</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('superAdmin.employes.summary') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Summary</p>
                  </a>
                </li>               
              </ul>                  
          </li>
        </ul>
        
      </div>  

      <div class="user-panel">
        <span class="brand-text font-weight-light text-sm text-white">HRD Menu</span>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">         
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Management User
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('superAdmin.user.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Registration User</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('superAdmin.employee.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Employee</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Summary</p>
                  </a>
                </li>               
              </ul>                  
          </li>
        </ul>
        
      </div>       

      </nav>
     
    </div>
  
  </aside>