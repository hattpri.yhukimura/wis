@extends('admin.hrd.templates.default')

@section('content')

<div class="row">
	<div class="col-sm-3">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fas fa-users"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Employes</span>
          <span class="info-box-number ">{{ $employes->count() }} actived</span>
          <span class="progress-description" style="text-align: right; font-size: 12px;">
            <a href="{{ route('admin.hrd.employee.index') }}" class="text-white">Detail</a>
          </span>
        </div>
      </div>
	</div>
	<div class="col-sm-5">
      <div class="info-box bg-blue">
        <span class="info-box-icon"><i class="fas fa-user-alt"></i></span>
        <div class="info-box-content col-sm-4">
          <span class="info-box-text">Contract</span>
          <span class="info-box-number">{{ $employes->where('emp_status', 'contract')->count() }} Employes</span>          
        </div>
        <div class="info-box-content col-sm-4">
          <span class="info-box-text">Permanent</span>
          <span class="info-box-number">{{ $employes->where('emp_status', 'permanent')->count() }} Employes</span>        
        </div>
        <div class="info-box-content col-sm-4">
          <span class="info-box-text">Internship</span>
          <span class="info-box-number">{{ $employes->where('emp_status', 'internship')->count() }} Employes</span>        
        </div>       
      </div>
	</div>
</div>

@endsection