@extends('admin.hrd.templates.default')

@section('content')

<div class="card">
    <div class="card-header">
         <h3 class="card-title">Data Employes</h3>   
         <a class="btn btn-outline-primary btn-sm float-right" href="{{{ route('admin.hrd.employee.create') }}}">Add</a>         
    </div>	
    <div class="card-body">
		<h4>Employes Active</h4>
		<div class="table-responsive table-sm">
			<table id="employesDataActive" class="table table-bordered table-hover text-sm">
			 	<thead>
			 		<tr>
			 			<th>No</th>
			 			<th>Nik</th>
			 			<th>Name</th>
			 			<th>Gender</th>
			 			<th>Departement</th>
			 			<th>Position</th>
			 			<th>Join Date</th>
			 			<th>End Date</th>
			 			<th>Status</th>
			 			<th>Action</th>	 			
			 		</tr>
			 	</thead>		 	
			</table>
		</div>
    </div>

	<div class="card-body">
		<h4>Employes Non-active</h4>
		<div class="table-responsive table-sm">
			<table id="employesDataNonActive" class="table table-bordered table-hover text-sm">
			 	<thead>
			 		<tr>
			 			<th>No</th>
			 			<th>Nik</th>
			 			<th>Name</th>
			 			<th>Gender</th>
			 			<th>Departement</th>
			 			<th>Position</th>
			 			<th>Join Date</th>
			 			<th>End Date</th>
			 			<th>Status</th>
			 			<th>Action</th>	 			
			 		</tr>
			 	</thead>		 	
			</table>
		</div>
    </div>

</div>
<form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Hapus" style="display: none">
</form>
<div class="modal fade bd-example-modal-lg" id="showEmployes" tabindex="-1" role="dialog" aria-labelledby="showEmployesLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document" id="contentModal">
  	    
  </div>
</div>
@endsection
@push('styles')
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">
@endpush


@push('jquery')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>


	
	{{-- fitur excel --}}
	<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/DataTables/JSZip/jszip.min.js') }}"></script>
@endpush

@push('scripts')
	@include('admin.hrd.templates.partials.alerts')
<script>
	$(function () {
		$('#employesDataActive').DataTable({
		 		processing: true,
                serverSide: true,
                ajax: '{{ route('admin.hrd.employee.data.active') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},                
                    { data: 'nik'} ,
                    { data: 'full_name'},
                    { data: 'gender'},
                    { data: 'department_id'},   
                    { data: 'position'},
                    { data: 'start_date'},
                    { data: 'end_date'},
                    { data: 'emp_status'},
                    { data: 'action'},         
                ],
				search: {
					"regex": true
				},
			    dom: 'Bfrtip',
		        buttons: [
		             'excel'
		        ]                 
		});

		$('#employesDataNonActive').DataTable({
		 		processing: true,
                serverSide: true,
                ajax: '{{ route('admin.hrd.employee.data.nonActive') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},                
                    { data: 'nik'} ,
                    { data: 'full_name'},
                    { data: 'gender'},
                    { data: 'department_id'},   
                    { data: 'position'},
                    { data: 'start_date'},
                    { data: 'end_date'},
                    { data: 'emp_status'},
                    { data: 'action'},         
                ],
				search: {
					"regex": true
				},
			    dom: 'Bfrtip',
		        buttons: [
		             'excel'
		        ]                 
		});
	});

</script>
@endpush