<!DOCTYPE html>
<html>

@include('admin.hrd.templates.partials.head')

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  @include('admin.hrd.templates.partials.navbar')

 @include('admin.hrd.templates.partials.leftSideBar')


  <div class="content-wrapper">   
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1 class="m-0 text-dark">{{ Breadcrumbs::current()->title }}</h1>
          </div>
          <div class="col-sm-6">
             <ol class="breadcrumb float-sm-right btn-sm">             
              {{ Breadcrumbs::render() }}            
             </ol>
          </div>
        </div>
      </div>
    </div>
   
    <section class="content">
      <div class="container-fluid">
      
        @yield('content')
      
      </div>
    </section>
  </div>

  @include('admin.hrd.templates.partials.footer')

 
  <aside class="control-sidebar control-sidebar-dark">   
  </aside>
 
</div>

  @include('admin.hrd.templates.partials.jquery')
  
</body>
</html>
